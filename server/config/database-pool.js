const dbConfig = require('../config/database'),
      mysql    = require('promise-mysql');

module.exports = mysql.createPool({
    connectionLimit : 50,
    host            : dbConfig.connection.host,
    user            : dbConfig.connection.user,
    password        : dbConfig.connection.password,
    database        : dbConfig.database
});
