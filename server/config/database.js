// config/database.js
module.exports = {
    'connection': {
        'host': 'localhost',
        'user': 'root',
        'password': 'toor'
    },
    'database': 'game-house',
    'users_table': 'users',
    'items_table': 'items'
};