var utils              = require('../utils'),
    RememberMeStrategy = require('passport-remember-me-extended').Strategy;


module.exports = function (passport, Users) {
    passport.use(new RememberMeStrategy(
        function(token, done) {
            Users.consumeRememberMeToken(token, function (err, user) {
                if (err) { return done(err); }
                if (!user) { return done(null, false); }
                return done(null, user);
            });
        },
        issueToken
    ));

    function issueToken(user) {
        var token = utils.randomString(64);
        return new Promise(function(resolve, reject) {
            Users.saveRememberMeToken(token, { userId: user.id }, function(err) {
                if (err) { return reject(err); }
                return resolve(null, token);
            });
        });
    }

    return {
        issueToken: issueToken
    };
};

