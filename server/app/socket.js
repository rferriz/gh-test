const Users = require('./service/users'),
      Items = require('./service/items'),
      util  = require('util'),
      _     = require('lodash'),
      uuid  = require('uuid');

const MAX_ITEM_DESCRIPTION = 100;

var shoplist_stats = {};

exports.run = function(SocketIO, socket) {
    printSocketInfo(socket, '[INFO] A user connected');
    socket.userData = {};

    socket.on('disconnect', () => {
        printSocketInfo(socket, '[INFO] User disconnected');
        socket.userData = {};
    });

    ////////////////////
    // Authentication //
    ////////////////////
    socket.on('authenticate', (data) => {
        printSocketInfo(socket, '[DEBUG] New authentication requested', data);
        socket.userData = null;

        Users.findUserByIdToken(data.idToken, (err, user) => {
            if (err)
                return printSocketInfo(socket, '[ERROR] New authentication', err);

            if (user) {
                socket.userData = _.pick(user, 'id', 'username', 'display_name', 'id_token');
                printSocketInfo(socket, '<== User authenticated ' + user.username);
                socket.emit('authenticated', {user_id: socket.userData.id});
            } else {
                socket.emit('not-authenticated');
                printSocketInfo(socket, '<== Authentication rejected');
            }
        });
    });

    socket.on('me', () => {
        if (!socket.userData) {
            return socket.emit('not-authenticated');
        }

        let filter = {
            id: socket.userData.id
        };

        //printSocketInfo(socket, '[DEBUG] me', filter);

        Users.find(filter)
            .then((data) => {
                let userData = _.pick(data[0], 'id', 'username', 'email');

                printSocketInfo(socket, null, userData);
                socket.emit('me', userData);
            })
            .catch((error) => {
                printSocketInfo(socket, '[ERROR] Get information about myself', error);
            });
    });

    ////////////////////
    //    Dashboard   //
    ////////////////////
    socket.on('enter-dashboard', () => {
        socket.join('dashboard', (err) => {
            if (err)
                return printSocketInfo(socket, '[ERROR] Enter dashboard', err);
            printSocketInfo(socket, 'Rooms',  socket.rooms);
            socket.emit('dashboard', shoplist_stats);
        });
    });

    socket.on('leave-dashboard', () => {
        socket.leave('dashboard', (err) => {
            if (err)
                printSocketInfo(socket, '[ERROR] leave-dashboard', err);
        });
    });

    // Emit dashboard every 1000ms in dashboard channel
    setInterval(() => {
        SocketIO.to('dashboard').emit('dashboard', shoplist_stats);
    }, 1000);

    socket.on('random-listing', () => {
        printSocketInfo(socket, '[INFO] Requested random listing');
        try {

            if (!('dashboard' in socket.rooms)) {
                printSocketInfo(socket, '[ERROR] Not subscribed to dashboard channel');
                socket.emit('error', {message: 'Not subscribed to the channel'});
                return;
            }

            Items.getRandomLists(12)
                .then(data => {
                    printSocketInfo(socket, '[DEBUG] Shop-listing found', data);
                    let payload = [];
                    for (let i = 0; i < data.length; i++) {
                        let payloadData = _.pick(data[i], 'user_id', 'TotalItems');
                        payloadData.displayName = data[i].display_name;
                        printSocketInfo(socket, payloadData);
                        payload.push(payloadData);
                    }

                    socket.emit('random-shop-listing', payload);
                })
                .catch(err => {
                    printSocketInfo(socket, '[ERROR] Get random shop-listings', err);
                    socket.emit('error', {message: 'Error getting random shop-listings'});
                })
        } catch (ex) {
            printSocketInfo(socket, '[ERROR] Exception', ex);
        }
    });

    ////////////////////
    // Shopping list  //
    ////////////////////
    socket.on('enter-shop-listing', (dto) => {
        printSocketInfo(socket, 'Joining shop listing #' + dto.id);

        socket.join(dto.id, (err) => {
            if (err)
                return printSocketInfo(socket, '[ERROR] Joining shop listing #' + dto.id, err);

            let userFilter = {id: dto.id};
            var cartFilter = {user_id: dto.id};
            var output = {user_id: dto.id};

            Users.find(userFilter)
                .then((data) => {
                    output.user = data[0];
                    return Items.find(cartFilter)
                })
                .then((data) => {
                    output.items = data;
                    socket.emit('shop-listing', output);
                })
                .catch((err) => {
                    printSocketInfo(socket, '[ERROR] Joining shop listing #' + dto.id, err);
                });
        });
    });

    socket.on('leave-shop-listing', (dto) => {
        printSocketInfo(socket, 'Leaving shop list #' + dto.id);

        socket.leave(dto.id, (err) => {
            if (err)
                printSocketInfo(socket, '[ERROR] leave-shop-list', err);
        });
    });


    /////////////////////////////
    // Shop listing management //
    /////////////////////////////
    socket.on('add-item', (data) => {
        printSocketInfo(socket, 'Add item request');

        if (!socket.userData) {
            socket.emit('not-authenticated');
            return printSocketInfo(socket, '[ERROR] add-item rejected: Not authenticated');
        }

        if (socket.userData.id != data.id) {
            printSocketInfo(socket, '[ERROR] User ' + socket.userData.id + ' tried to update shopping list of user ' + data.id, data);
            return socket.emit('error', {message: 'Not your shopping list'});
        }

        if (!(socket.userData.id in socket.rooms)) {
            printSocketInfo(socket, '[ERROR] User is not subscribed to its channel #' + socket.userData.id, data);
            return socket.emit('error', {message: 'Not subscribed to the channel'});
        }

        var roomId = data.id;
        var item = {
            user_id: parseInt(roomId),
            id: uuid.v4(),
            description: data.description.substring(0, MAX_ITEM_DESCRIPTION),
            created: new Date()
        };

        Items.add(item, err => {
            if (err)
                return printSocketInfo(socket, '[ERROR] add-item.find', err);
            shoplist_stats.items++;

            var message = {
                user_id: parseInt(roomId),
                item: _.omit(item, 'user_id')
            };

            // printSocketInfo(socket, '[DEBUG] Item added #' + item.id);
            SocketIO.to(roomId).emit('item-added', message);
        });
    });

    socket.on('update-item', (data) => {
        printSocketInfo(socket);
        if (!socket.userData) {
            socket.emit('not-authenticated');
            return printSocketInfo(socket, '[ERROR] add-item rejected: Not authenticated');
        }

        if (!(socket.userData.id in socket.rooms)) {
            printSocketInfo(socket, '[ERROR] User is not subscribed to its channel #' + socket.userData.id, data);
            return socket.emit('error', {message: 'Not subscribed to the channel'});
        }

        var roomId = socket.userData.id;
        var filter = {
            user_id: roomId,
            id: data.id
        };

        var updateMsg = {
            user_id: parseInt(roomId),
            item: {
                id: data.id,
                description: data.description.substring(0, MAX_ITEM_DESCRIPTION)
            }
        };

        Items.find(filter)
            .then(done => {
                //printSocketInfo(socket, '[DEBUG] update-item: find', done);
                if (done) {
                    // printSocketInfo(socket, '[DEBUG] Update item #' + filter.id);
                    return Items.update(data.id, {description: updateMsg.item.description});
                }
            })
            .then(() => {
                // printSocketInfo(socket, '[DEBUG] Item updated #' + data.id);
                SocketIO.to(roomId).emit('item-updated', updateMsg);
            })
            .catch((err) => {
                printSocketInfo(socket, '[ERROR] update-item', err);
            });
    });

    socket.on('delete-item', (data) => {
        printSocketInfo(socket);
        if (!socket.userData) {
            // printSocketInfo(socket, '[DEBUG] delete-item: Not authenticated');
            return socket.emit('not-authenticated');
        }

        if (!(socket.userData.id in socket.rooms)) {
            printSocketInfo(socket, '[ERROR] User is not subscribed to its channel #' + socket.userData.id);
            return socket.emit('error', {message: 'Not subscribed to the channel'});
        }

        if (typeof (data.id) === 'undefined') {
            return printSocketInfo(socket, '[ERROR] Invalid input data', data);
        }

        var roomId = socket.userData.id;
        var item = {
            user_id: roomId,
            id: data.id
        };

        var deleteMsg = {
            user_id: parseInt(roomId),
            item: {
                id: item.id
            }
        };

        Items.find(item)
            .then(rows => {
                if (!rows)
                    return printSocketInfo(socket, '[INFO] Item not found: ' + item.id);

                if (!rows.length)
                    return printSocketInfo(socket, '[INFO] Item not found: ' + item.id);

                // printSocketInfo(socket, '[DEBUG] Item ready to delete: ' + item.id);
                return Items.remove(item);
            })
            .then(() => {
                //printSocketInfo(socket, '[DEBUG] Item deleted :' + item.id);
                shoplist_stats.items--;
                SocketIO.to(roomId).emit('item-deleted', deleteMsg);
            })
            .catch((err) => {
                printSocketInfo(socket, '[ERROR] Trying to delete item', err);
            });
    });

    // Emit not authorized to force authorization from clients
    // printSocketInfo(socket, '[DEBUG] Sending not-authenticated event');
    socket.emit('not-authenticated');
    socket.emit('reconnected');
};

exports.installPopulateStats = () => {
    console.log('Install populate stats job (sync every 10s)');
    populateStats();
    return setInterval(() => {
        populateStats();
    }, 10000);
};

function populateStats() {
    // console.log('[DEBUG] Populating stats...');

    Users.count().then(data => {
        // console.log('[DEBUG] Total users: ' + data);
        shoplist_stats.users = data;
    }).catch(err => {
        console.error(err);
    });

    Items.itemCount().then(data => {
        // console.log('[DEBUG] Total item count: ' + data);
        shoplist_stats.items = data;
    }).catch(err => {
        console.error(err);
    });

    Items.cartCount().then(data => {
        // console.log('[DEBUG] Total shopping list: ' + data);
        shoplist_stats.carts = data;
    }).catch(err => {
        console.error(err);
    });
}

function printSocketInfo(socket, message, extra) {
    var socketInfo = _.pick(socket, 'id', 'userData', 'rooms');

    if (typeof socketInfo !== 'undefined') {
        if ((typeof (socketInfo.userData) !== 'undefined') && socketInfo.userData !== null) {
            socketInfo.userData = socketInfo.userData.id;
        }
    }

    var msgObject = {
        message: message,
        socket: socketInfo,
        extra: extra
    };

    console.log(util.inspect(msgObject, false, 2, true));
}
