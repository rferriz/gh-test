const _         = require('lodash'),
      utils     = require('../../utils'),
      jwt       = require('jsonwebtoken'),
      jwtConfig = require('../../config/jwt'),
      dbConfig  = require('../../config/database'),
      pool      = require('../../config/database-pool'),
      bcrypt    = require('bcrypt-nodejs');

exports.count = function () {
    return new Promise(function (resolve, reject) {
        var selectQuery = 'SELECT count(*) as total from`' + dbConfig.database + '`.`' + dbConfig.users_table + '`';
        pool.query(selectQuery)
            .then(function (rows) {
                resolve(rows[0].total);
            })
            .catch(function (err) {
                reject(err)
            });
    });
};

exports.find = function(filter) {
    var selectQuery ='SELECT * FROM `' + dbConfig.database + '`.`' + dbConfig.users_table+ '` WHERE ';
    var wheres = [];
    var criteria = [];

    if (filter.id) {
        wheres.push(' id=? ');
        criteria.push(filter.id);
    }

    if (filter.id_token) {
        wheres.push(' id_token=? ');
        criteria.push(filter.id_token);
    }

    selectQuery = selectQuery + wheres.join(' AND ');

    return new Promise(function (resolve, reject) {
        pool.query(selectQuery, criteria)
            .then(function(data) {
                if (!data.length) {
                    return resolve([]);
                } else {
                    return resolve(data);
                }
            })
            .catch(function (err) {
                return reject(err);
            });
    });
};

exports.findUserByIdToken = function (token, done) {
    var selectQuery ='SELECT * FROM `' + dbConfig.database + '`.`' + dbConfig.users_table+ '` WHERE id_token = ?';

    return pool.query(selectQuery, [token], function (err, rows) {
        if (err)
            return done(err, null);

        if (!rows.length)
            return done('Id Token not found', null);

        if (rows.length > 1)
            return done('Too many users with same id token', null);

        return done(null, rows[0]);
    });
};

exports.consumeRememberMeToken = function (token, fn) {
    var selectQuery ='SELECT * FROM `' + dbConfig.database + '`.`' + dbConfig.users_table+ '` WHERE remember_me = ?';
    return pool.query(selectQuery, [token], function (err, rows) {
        if (err)
            return fn(err, null);

        if (!rows.length || rows.length > 1)
            return fn(null, null);

        var uid = rows[0].id;
        // Delete token
        exports.saveRememberMeToken(null, uid);

        return fn(null, rows[0].id);
    });
};

exports.saveRememberMeToken = function (token, uid, fn) {
    var updateQuery;
    if (!token) {
        updateQuery = 'UPDATE `' + dbConfig.database + '`.`' + dbConfig.users_table + '` SET remember_me = null WHERE id = ?';
        return pool.query(updateQuery, [uid], function (err, rows) {
            return fn(err, rows);
        });
    } else {
         updateQuery = 'UPDATE `' + dbConfig.database + '`.`' + dbConfig.users_table + '` SET remember_me = ? WHERE id = ?';
        return pool.query(updateQuery, [token, uid], function (err, rows) {
            return fn(err, rows);
        });
    }
};

exports.generateAndGetTokens = function * (inUser) {
    return new Promise(function (resolve, reject) {
        var user = _.pick(inUser, 'id', 'username', 'display_name');

        var idToken = createIdToken(user);
        var accessToken = createAccessToken(inUser.username);

        var updateQuery = 'UPDATE `' + dbConfig.users_table + '` SET id_token = ? where id = ?';
        pool.query(updateQuery, [idToken, user.id])
            .then(function () {
                resolve({
                    id_token: idToken,
                    access_token: accessToken
                });
            })
            .catch(function (err) {
                reject(err);
            });
    });
};

exports.createNew = function (inUser) {
    return new Promise((resolve, reject) => {
        pool.query("SELECT * FROM `" + dbConfig.users_table + "` WHERE username = ?", [inUser.username], function (err, rows) {
            if (err)
                return reject(err);

            if (rows.length) {
                //console.log('[INFO] Same username found');
                return resolve(false); //, req.flash('signupMessage', 'That username is already taken.'));
            }

            //console.log('[DEBUG] New user');
            // if there is no user with that username
            // create the user
            var newUserMysql = {
                username: inUser.username,
                display_name: inUser.display_name,
                password: bcrypt.hashSync(inUser.password, null, null)  // use the generateHash function in our user model
            };

            let insertQuery = "INSERT INTO `" + dbConfig.users_table + "` ( username, display_name, password ) values (?,?,?)";

            pool.query(insertQuery, [newUserMysql.username, newUserMysql.display_name, newUserMysql.password], function (err, rows) {
                if (err)
                    return reject(err);

                newUserMysql.id = rows.insertId;
                return resolve(newUserMysql);
            });
        });
    });
};

function createIdToken(user) {
    return jwt.sign(_.omit(user, 'password'), jwtConfig.secret, { expiresIn: 60*60*5 });
}

function createAccessToken(username) {
    return jwt.sign({
        iss: jwtConfig.issuer,
        aud: jwtConfig.audience,
        exp: Math.floor(Date.now() / 1000) + (60 * 60),
        scope: 'full_access',
        sub: username,
        jti: utils.randomString(16), // unique identifier for the token
        alg: 'HS256'
    }, jwtConfig.secret);
}