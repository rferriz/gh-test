var dbConfig = require('../../config/database'),
    pool     = require('../../config/database-pool');

exports.itemCount = function () {
    return new Promise(function (resolve, reject) {
        var selectQuery = 'SELECT count(*) as total from`' + dbConfig.database + '`.`' + dbConfig.items_table + '`';

        pool.query(selectQuery)
            .then(function (rows) {
                resolve(rows[0].total);
            })
            .catch(function (err) {
                reject(err)
            });
    });
};

exports.cartCount = function () {
    return new Promise(function (resolve, reject) {
        var selectQuery = 'SELECT count(distinct user_id) as total from`' + dbConfig.database + '`.`' + dbConfig.items_table + '`';

        pool.query(selectQuery)
            .then(function (rows) {
                resolve(rows[0].total);
            })
            .catch(function (err) {
                reject(err)
            });
    });
};

exports.add = function(item, done) {
    return new Promise((resolve, reject) => {
        let insertQuery = 'INSERT INTO `' + dbConfig.database + '`.`' + dbConfig.items_table + '` ( user_id, id, description, created ) values (?,?,?,?)';
        let createdMysql = toMySqlDatetime(item.created);

        pool.query(insertQuery,[item.user_id, item.id, item.description, item.created], (err, rows) => {
            if (err) {
                if (typeof done === 'function')
                    done(err, null);
                return reject(err);
            }
            if (typeof done === 'function')
                done(null, err);
            return resolve(rows);
        });
    });
};

exports.find_old = function (filter, done) {
    var selectQuery ='SELECT * FROM `' + dbConfig.database + '`.`' + dbConfig.items_table + '` WHERE ';
    var wheres = [];
    var criteria = [];

    if (filter.user_id) {
        wheres.push(' user_id=? ');
        criteria.push(filter.user_id);
    }

    if (filter.id) {
        wheres.push(' id=? ');
        criteria.push(filter.id);
    }

    selectQuery = selectQuery + wheres.join(' AND ');

    return pool.query(selectQuery, criteria, function (err, rows) {
        if (err)
            return done(err, null);

        if (!rows.length)
            return done('Not found', null);

        return done(null, rows);
    });
};

exports.find = function (filter) {
    var selectQuery ='SELECT * FROM `' + dbConfig.database + '`.`' + dbConfig.items_table + '` WHERE ';
    var wheres = [];
    var criteria = [];

    if (filter.user_id) {
        wheres.push(' user_id=? ');
        criteria.push(filter.user_id);
    }

    if (filter.id) {
        wheres.push(' id=? ');
        criteria.push(filter.id);
    }

    selectQuery = selectQuery + wheres.join(' AND ');

    return new Promise(function (resolve, reject) {
        return pool.query(selectQuery, criteria)
            .then(function(data) {
                if (!data.length) {
                    return resolve([]);
                } else {
                    return resolve(data);
                }
            })
            .catch(function (err) {
                //console.error(err);
                return reject(err);
            });
    });
};

exports.getRandomLists = function (number) {
    //var selectQuery = 'SELECT user_id, count(id) as TotalItems FROM `' + dbConfig.database + '`.`' + dbConfig.items_table + '` GROUP BY user_id ORDER BY RAND() LIMIT ' + number;
    var selectQuery = 'SELECT calc.user_id, users.display_name, calc.TotalItems \
        FROM \
        (SELECT user_id, COUNT(items.id) AS TotalItems FROM `' + dbConfig.database + '`.`' + dbConfig.items_table + '` as items GROUP BY user_id) calc, \
        `' + dbConfig.database + '`.`' + dbConfig.users_table + '` as users \
        where calc.user_id = users.id \
        ORDER BY RAND() limit ' + number;

    return new Promise((resolve, reject) => {
        pool.query(selectQuery)
            .then(data => {
                resolve(data);
            })
            .catch(err => {
                reject(err);
            });
    });
};

exports.update = function (id, data) {
    var updateQuery ='UPDATE `' + dbConfig.database + '`.`' + dbConfig.items_table + '` SET ';
    var setString = [];
    var criteria = [];

    if (data.description) {
        setString.push(' description=? ');
        criteria.push(data.description);
    }
    updateQuery = updateQuery + setString.join(',') + ' WHERE id=?';
    criteria.push(id);

    return pool.query(updateQuery, criteria);
};

exports.remove = function(filter, done) {
    var deleteQuery ='DELETE FROM `' + dbConfig.database + '`.`' + dbConfig.items_table + '` WHERE ';
    var wheres = [];
    var criteria = [];

    if (filter.user_id) {
        wheres.push(' user_id=? ');
        criteria.push(filter.user_id);
    }

    if (filter.id) {
        wheres.push(' id=? ');
        criteria.push(filter.id);
    }

    deleteQuery = deleteQuery + wheres.join(' AND ');

    return pool.query(deleteQuery, criteria);
};

function toMySqlDatetime(date)
{
    return (
        new Date ((new Date((new Date(date)).toISOString())).getTime() - (date.getTimezoneOffset()*60000)))
        .toISOString()
        .slice(0, 19)
        .replace('T', ' ');
}