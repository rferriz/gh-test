var express   = require('express'),
    _         = require('lodash'),
    pool      = require('../config/database-pool'),
    utils     = require('../utils');


module.exports = function(app, passport, Users, RememberMe) {

    app.post('/users', passport.authenticate('local-signup'), function * (req, res) {
        // Authentication succeed
        RememberMe.issueToken(req.user, function (err) {
            console.error(err);
        });

        try {
            var tokens = yield Users.generateAndGetTokens(req.user);
            var dto = _.pick(tokens, 'id_token', 'access_token', 'remember_me');
            res.status(201).send(dto);
        } catch (err) {
            console.error(err);
            res.status(500).send("Error saving access_token");
        }
    });

    app.post('/auth/login', passport.authenticate('local-login'), function * (req, res) {
        // Authentication succeed
        console.log('Before remember me check');
        if (req.body && req.body.remember_me) {
            var token;
            try {
                token = yield RememberMe.issueToken(req.user);
                res.cookie('remember_me', token, {path: '/', maxAge: 3600 * 24 * 14});
            }
            catch (error) {
                console.error(error);
            }
        }

        try {
            var tokens = yield Users.generateAndGetTokens(req.user);
            var dto = _.pick(tokens, 'id_token', 'access_token', 'remember_me');
            res.status(200).send(dto);
        } catch (err) {
            console.error(err);
            res.status(500).send("Error saving access_token");
        }
    });
};
