var mysql    = require('mysql'),
    dbConfig = require('../config/database');

var connection = mysql.createConnection(dbConfig.connection);

connection.query('CREATE DATABASE `' + dbConfig.database + '`');

connection.query('\
    CREATE TABLE `' + dbConfig.database + '`.`' + dbConfig.users_table + '` ( \
        `id` int NOT NULL AUTO_INCREMENT, \
        `username` varchar(100) NOT NULL, \
        `password` varchar(100) NOT NULL, \
        `display_name` varchar(255) NOT NULL, \
        `id_token` varchar(255), \
        `remember_me` varchar(255), \
        PRIMARY KEY (`id`), \
        UNIQUE KEY `unique_username` (`username`) \
    )');

connection.query('\
    CREATE TABLE `' + dbConfig.database + '`.`' + dbConfig.items_table + '` ( \
        `id` varchar(100) NOT NULL, \
        `user_id` int NOT NULL, \
        `description` varchar(100) NOT NULL, \
        `created` TIMESTAMP NOT NULL, \
        PRIMARY KEY (`id`) \
    )');

connection.query('\
    ALTER TABLE `' + dbConfig.database + '`.`' + dbConfig.items_table + '` \
    ADD FOREIGN KEY (`user_id`) \
    REFERENCES `' + dbConfig.database + '`.`' + dbConfig.users_table + '` (`id`) \
');

console.log('Success: Database created');

connection.end();
