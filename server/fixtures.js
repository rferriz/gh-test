const Users = require('./app/service/users');
const Items = require('./app/service/items');
const utils = require('util');
const uuid  = require('uuid');
const dbConfig = require('./config/database');
const pool = require('./config/database-pool');
const async = require('asyncawait/async');
const await = require('asyncawait/await');


var Names = [
    'John', 'Ed', 'Adam', 'Eve', 'Mike', 'Joe', 'Lis', 'George', 'Donna', 'Angy', 'Indy', 'Carol', 'Rob', 'Robert', 'Quay',
    'Sarah', 'Tom', 'Sandra', 'Alex', 'Leonard', 'Penny', 'Howard', 'Peter', 'Bob', 'Rick', 'Zoe', 'Susan', 'Rupert', 'Fer'
];
var Surnames = [
    'Doe', 'Miller', 'Mulder', 'Scaly', 'Sullivan', 'Cocker', 'Slate', 'Washintong', 'Indiana', 'Nevada', 'Alker', 'Plain',
    'Paff', 'Po', 'Perry', 'Persitt', 'Petrakon', 'Wolowitz', 'Cooper', 'Hoffstader', 'Griffin', 'Yoel', 'Susu', 'Molokev'
];

var ItemData = [
    'Eggs', 'Milk', 'Soap', 'Jam', 'Guitar', 'Water', 'Oranges', 'Pizza', 'Tablet', 'Biscuits', 'Tofee', 'Ham', 'Juice',
    'Keyboard', 'Mouse', 'Plant', 'Water', 'Coke', 'Beer'
];


var usersCreated = 0;
var itemsCreated = 0;

function randomObjectName() {
    let name = Names[Math.floor(Math.random() * Names.length)];
    let surn = Surnames[Math.floor(Math.random() * Surnames.length)];
    return {name: name, surname: surn};
}

function usernameFromObjectName(object) {
    let serialized = object.name + '.' + object.surname;
    return serialized.toLowerCase();
}

function randomItemDataObject() {
    return {
        id: uuid.v4(),
        description: ItemData[Math.floor(Math.random() * ItemData.length)],
        created: new Date()
    };
}

var ma = exports;

exports.cleanMe = async(function () {
    return new Promise((resolve, reject) => {
        let deleteSql = 'DELETE FROM `' + dbConfig.database + '`.`' + dbConfig.items_table + '`;';
        let deleteUserSql = 'DELETE FROM `' + dbConfig.database + '`.`' + dbConfig.users_table + '`;';

        try {
            console.log(' => Empty items');
            await(pool.query(deleteSql));

            console.log(' => Empty users');
            await(pool.query(deleteUserSql));

            resolve(true);
        } catch (ex) {
            reject(ex);
        }
    });
});

exports.run = async(function (numUsers, maxItemsPerUser) {
    return new Promise((resolve, reject) => {
        let idx = 0;
        while (idx < numUsers) {
            try {
                await(ma.createRandomUser(maxItemsPerUser));
                idx++;
                usersCreated++;
            } catch (ex) { }
        }
        resolve(true);
    });
});

exports.createRandomUser = async(function (maxItemsPerUser) {
    return new Promise((resolve, reject) => {
        let name;
        let username;
        let displayName;

        name = randomObjectName();
        username = usernameFromObjectName(name);
        displayName = name.name + ' ' + name.surname;

        let userData = {
            username: username + '@email.com',
            display_name: displayName,
            password: username
        };

        try {
            let user = await (Users.createNew(userData));
            if (user === false) return reject(false);
            console.log(' => Usuario #' + (usersCreated + 1));
            let numItems = Math.floor(Math.random() * maxItemsPerUser);
            for (let j = 0; j < numItems; j++) {
                try {
                    await(module.exports.createRandomItem(user.id));
                } catch (ex) { }
            }
        } catch (err) {
            // console.error('[ERROR] ' + utils.inspect(ex, false, 2, true));
            reject(err);
        }

        resolve(true);
    });
});

exports.createRandomItem = async (function (userId) {
    return new Promise((resolve, reject) => {
        // console.log('createRandomItem');
        let itemData = randomItemDataObject();
        itemData.user_id = userId;

        try {
            await(Items.add(itemData));
            itemsCreated++;
            resolve(true);
        } catch (ex) {
            //console.error('[ERROR-RETRY] ' + utils.inspect(ex, false, 2, true));
            reject(ex);
        }
    });
});

var num = 100;

var fixtures = async(function(numUsers) {
    try {
        console.log('=> Clean');
        var clean = await (ma.cleanMe());

        console.log('=> Create users');
        var users = await (ma.run(numUsers, 15));

        return 'Users created: ' + usersCreated + "\n" + 'Items created: ' + itemsCreated;

    } catch(ex) {
        console.log('Caught an error');
    }

    return 'Finished';
});


fixtures(num)
    .then(function(result) {
        console.log(result);
        setInterval(() => {
            process.exit();
        }, 3000);
    })
    .catch(function(err) {
        console.error(err);
    });