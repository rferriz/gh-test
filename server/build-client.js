var child_process   = require('child_process');

module.exports = {

    build : function () {
        console.log('Building aurelia app...');
        child_process.execSync('cd ../client; gulp build', {
            stdio: 'inherit'
        });
        console.log('Building complete');
    }
};