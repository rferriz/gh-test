const express = require('express');
const yields = require('express-yields');
var clientBuilder   = require('./build-client'),
    passport        = require('passport'),
    bodyParser      = require('body-parser'),
    flash           = require('connect-flash'),
    app             = express(),
    socket          = require('./app/socket'),
    path            = require('path'),
    Users           = require('./app/service/users'),
    http            = require('http').Server(app),
    io              = require('socket.io')(http)
    ;

// Uncomment above line to build client
if (process.env.BUILD_CLIENT)
    clientBuilder.build();

require('./config/passport')(passport);
var rememberMe = require('./config/rememberme')(passport, Users);

var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type,Authorization');

    next();
};

app.use(allowCrossDomain);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(passport.initialize());
app.use(passport.session());

// Client App
app.use(express.static(path.join(__dirname, '../client')));

// Rest routes
require('./app/routes')(app, passport, Users, rememberMe);
io.on('connection', function(s) { socket.run(io, s); });

socket.installPopulateStats();

var port = process.env.PORT || 9900;

http.listen(port, function() {
    console.log('listening in http://localhost:' + port);
});
