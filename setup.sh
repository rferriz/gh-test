#!/usr/bin/env bash

echo "========================================================================================="
echo "This script setup the environment to run the applications"
echo "I will download all dependencies of client and server, and even create the MySql database"
echo " "
echo "Before running this script"
echo " "
echo " 1. Install MySql Server"
echo " 2. Update database settings on server/config/database.js"
echo " "
echo "WARNING: This script uses sudo to install nodejs, npm and jspm, you may be asked for your user password"
echo " "
echo "Press [Intro] to continue or [Ctrl+C] to abort"
read

echo "===> Install NodeJs"
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

echo "===> Update npm"
sudo npm install -g npm

echo "===> Install jspm"
sudo npm install -g jspm


echo "===> Install dependencies on client"
cd client
npm install
jspm install
echo "==> Install gulp"
npm install gulp

echo "===> Building client"
gulp build
cd ..

echo "===> Install dependencies on server"
cd server
npm install
echo "===> Create MySql database"
echo " If this step fails, please: "
echo "  1. Update settings at 'server/config/database.js'"
echo "  2. Run 'node server/scripts/create_database.js'"
node scripts/create_database.js
echo "===> Setup fixtures"
echo " This step may take some minutes..."
node fixtures.js
cd ..

echo "===> Installation succeed"
echo " "
echo "To start the server:"
echo " $ cd server"
echo " $ PORT=80 sudo nodejs server.js"
echo " "
