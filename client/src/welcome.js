import {inject} from 'aurelia-framework';
import {HttpClient} from 'aurelia-fetch-client';
import {ClientSocket} from 'client-socket';

@inject(HttpClient, ClientSocket)
export class Welcome {
  heading = 'Welcome to the GameHouse Shopping-list App!';
  http = null;

  dashboard = {
    carts: 0,
    items: 0,
    users: 0
  };
  random = [];

  constructor(http, clientSocket) {
    this.http = http;
    this.clientSocket = clientSocket;
  }

  activate() {
    var self = this;
    this.clientSocket.getSocket().on('dashboard', response => {
      //console.log(response);
      this.dashboard = response;
    });
    this.clientSocket.getSocket().on('random-shop-listing', response => {
      //console.log(response);
      this.random = response;
    });
    this.clientSocket.getSocket().emit('enter-dashboard');
    setTimeout(function () {
      self.clientSocket.getSocket().emit('random-listing');
    }, 100);
  }

  deactivate() {
    this.clientSocket.getSocket().off('dashboard');
    this.clientSocket.getSocket().emit('leave-dashboard');
  }
}
