var config = {

    // Our Node API is being served from localhost:9900
    baseUrl: 'http://localhost:9900',
    // The API specifies that new users register at the POST /users enpoint.
    signupUrl: 'users',
    // Logins happen at the POST /auth/login endpoint.
    loginUrl: 'auth/login',
    // The API serves its tokens with a key of id_token which differs from
    // aureliauth's standard.
    tokenName: 'id_token',
    // Once logged in, we want to redirect the user to the welcome view.
    loginRedirect: '#/shop-list'

};

export default config;