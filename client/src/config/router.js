import {AuthorizeStep} from 'aurelia-auth';
import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';

@inject(Router)
export default class {

    constructor(router) {
        this.router = router;
    };

    configure() {

        var appRouterConfig = function(config) {

            config.title = 'GameHouse';

            // Here, we hook into the authorize extensibility point
            // to add a route filter so that we can require authentication
            // on certain routes
            config.addPipelineStep('authorize', AuthorizeStep);

            // Here, we describe the routes we want along with information about them
            // such as which they are accessible at, which module they use, and whether
            // they should be placed in the navigation bar
            config.map([
                { route: ['','welcome'],                     moduleId: './welcome',      nav: true,  title:'Welcome' },
                { route: 'about',                            moduleId: './about',        nav: true,  title: 'About' },
                { route: 'docs',                             moduleId: './docs',         nav: true,  title: 'Documentation' },
                { route: 'signup',                           moduleId: './signup',       nav: false, title:'Signup' },
                { route: 'login',                            moduleId: './login',        nav: false, title:'Login' },
                { route: 'logout',                           moduleId: './logout',       nav: false, title:'Logout' },
                { route: 'shop-list/:id?', name: 'shoplist', moduleId: './cart',         nav: false, title:'Shopping list' },

            ]);
        };

        // The router is configured with what we specify in the appRouterConfig
        this.router.configure(appRouterConfig);

    };
}
