import {AuthService} from 'aurelia-auth';
import {DialogService} from 'aurelia-dialog';
import {Redirect} from 'aurelia-router';
import {ShopItem} from 'model/shop-item';
import {ClientSocket} from './client-socket';
import {Prompt} from './modals/prompt';
import {Dialog} from './modals/dialog';

import {inject} from 'aurelia-framework';

@inject(AuthService, ClientSocket, DialogService)

export class Cart {
    _isOwned = false;
    cart = null; // CartId
    user = {};   // CartOwner info
    items = [];
    clientSocket = {};
    socket = {};
    share_url = "";
    sort_field = 'description';
    sort_direction = 'ascending';

    constructor(authService, clientSocket, dialogService) {
        this.authService = authService;
        this.clientSocket = clientSocket;
        this.dialogService = dialogService;
    }

    canActivate(params) {
        var self = this;

        return new Promise((resolve, reject) => {
            if (typeof params.id === 'undefined') {
                setTimeout(function() {
                    if (self.clientSocket.getUserId() == null) {
                        resolve(new Redirect('login'));
                    } else
                        resolve(new Redirect('shop-list/' + self.clientSocket.getUserId()));
                }, 100);
            } else {
                resolve(true);
            }
        });
    }

    activate(params) {
        this.clientSocket.getSocket().on('item-added', this.handleAddItem);
        this.clientSocket.getSocket().on('item-updated', this.handleUpdateItem);
        this.clientSocket.getSocket().on('item-deleted', this.handleDeleteItem);
        this.clientSocket.getSocket().on('shop-listing', this.handleShopListing);
        this.clientSocket.getSocket().on('reconnected', () => {
            setTimeout(() => {
                console.log('Reconnect to ' + this.cart);
                this.clientSocket.getSocket().emit('enter-shop-listing', {id: this.cart});
            }, 50);
        });

        if (typeof params.id === 'undefined') {
            this.cart = this.clientSocket.getUserId();
        } else {
            this.cart = params.id;
        }

        this.clientSocket.getSocket().emit('enter-shop-listing', {id:this.cart});
        this.share_url = window.location.href;
    }

    deactivate() {
        this.clientSocket.getSocket().emit('leave-shop-listing', {id:this.cart});
        this.clientSocket.getSocket().off('shop-listing');
        this.clientSocket.getSocket().off('item-added');
        this.clientSocket.getSocket().off('item-updated');
        this.clientSocket.getSocket().off('item-deleted');
        this.clientSocket.getSocket().off('reconnected');
    }

    addItem() {
        if (!this.isOwned)
            return;

        this.dialogService.open({
            viewModel: Prompt,
            model: 'Enter new shop item description:',
            lock: false
        }).whenClosed(response => {
            if (!response.wasCancelled) {
                this.clientSocket.addItem(this.cart, new ShopItem(null, response.output));
            }
        });
    }

    editItem(item) {
        if (!this.isOwned)
            return;

        this.dialogService.open({
            viewModel: Prompt,
            model: 'Rename' + item.description + ' to ...',
            lock: false
        }).whenClosed(response => {
            if (!response.wasCancelled) {
                this.clientSocket.updateItem(new ShopItem(item.id, response.output));
            }
        });
    }

    deleteItem(item) {
        if (!this.isOwned)
            return;

        this.dialogService.open({
            viewModel: Dialog,
            model: 'Are you sure to delete ' + item.description,
            lock: false
        }).whenClosed(response => {
            if (!response.wasCancelled) {
                this.clientSocket.removeItem(item)
            }
        });
    }

    handleAddItem = dtresp => {
        if (dtresp.user_id == this.cart) {
            var dtItem = dtresp.item;
            var item = new ShopItem(dtItem.id, dtItem.description, dtItem.created);
            let oldItems = this.items.slice(0);
            oldItems.push(item);
            this.applySort(oldItems);
        }
    };

    handleUpdateItem = dtresp => {
        if (dtresp.user_id == this.cart) {
            var dtItem = dtresp.item;
            var index = Cart.findById(this.items, dtItem.id);
            if (index > -1) {
                this.items[index].description = dtItem.description;
                this.applySort();
            }
        }
    };

    handleDeleteItem = dtresp => {
        if (dtresp.user_id == this.cart) {
            var dtItem = dtresp.item;
            if (dtItem.id === undefined) {
                console.error('Invalid item to delete');
                return;
            }

            for (var i = 0; i < this.items.length; i++) {
                var item = this.items[i];
                if (item.id == dtItem.id) {
                    console.log('Remove item #' + i);
                    this.items.splice(i, 1);
                }
            }
        }
    };

    handleShopListing = dtresp => {
        if (dtresp.user_id == this.cart) {
            this.user = dtresp.user;
            this.applySort(dtresp.items);
        }
    };

    get userId() {
        return this.clientSocket.getUserId();
    }

    get isOwned() {
        return parseInt(this.clientSocket.getUserId()) === parseInt(this.cart);
    }

    applySort(newItems) {
        if (!newItems)
            newItems = this.items;

        if (this.sort_field == 'description') {
            if (this.sort_direction == 'ascending') {
                this.items = newItems.sort(function (a, b) {
                    return (a.description.toUpperCase() > b.description.toUpperCase()) ? 1 : ((b.description.toUpperCase() > a.description.toUpperCase()) ? -1 : 0);
                });
            } else {
                this.items = newItems.sort(function (a, b) {
                    return (a.description.toUpperCase() < b.description.toUpperCase()) ? 1 : ((b.description.toUpperCase() < a.description.toUpperCase()) ? -1 : 0);
                });
            }
        } else if (this.sort_field == 'created') {
            if (this.sort_direction == 'ascending') {
                this.items = newItems.sort(function (a, b) {
                    return (a.created > b.created) ? 1 : ((b.created > a.created) ? -1 : 0);
                });
            } else {
                this.items = newItems.sort(function (a, b) {
                    return (a.created < b.created) ? 1 : ((b.created < a.created) ? -1 : 0);
                });
            }
        }
    }

    clickSort(field) {
        if (field == this.sort_field) {
            this.toggleSortDirection();
        } else  {
            this.sort_field = field;
            this.applySort();
        }
    }

    toggleSortDirection() {
        if (this.sort_direction == 'ascending') {
            this.sort_direction = 'descending';
        } else {
            this.sort_direction = 'ascending';
        }
        this.applySort();
    }

    static findById(array, id) {
        for (var index = 0; index < array.length; index++) {
            var item = array[index];
            if (item.id == id) {
                return index;
            }
        }
        return -1;
    }
}