import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';
import {ValidationControllerFactory, ValidationController, ValidationRules} from 'aurelia-validation';
import {BootstrapFormRenderer} from './bootstrap-form-renderer';

@inject(AuthService, ValidationControllerFactory)
export class Login {
    controller = null;
    heading = 'Log In';

    // These view models will be given values
    // from the login form user input
    email = '';
    password = '';

    // Any login errors will be reported by giving this view model a value in the catch block within the login method
    loginError = '';

    constructor(auth, controllerFactory) {
        this.auth = auth;
        this.controller = controllerFactory.createForCurrentScope();
        this.controller.addRenderer(new BootstrapFormRenderer());
    };

    login() {
        this.loginError = '';
        this.controller.validate()
            .then(result => {
                if (result.valid) {
                    return this.auth.login(this.email, this.password)
                        .then((response) => {
                            localStorage.setItem('access_token', response.access_token);
                        })
                        .catch(error => {
                            //console.error(error);
                            if (error.statusText === 'Unauthorized') {
                                this.loginError = error.statusText;
                            }
                        });
                }
            });
    };
}

ValidationRules
    .ensure(a => a.email).required().email()
    .ensure(a => a.password).minLength(6).required()
    .on(Login);