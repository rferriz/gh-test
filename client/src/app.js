import 'bootstrap';
//import 'bootstrap/css/bootstrap.css!';

import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {FetchConfig} from 'aurelia-auth';
import AppRouterConfig from './config/router';


@inject(Router, FetchConfig, AppRouterConfig)
export class App {

   constructor(router, fetchConfig, appRouterConfig) {

     this.router = router;

     // Client configuration provided by the aurelia-auth plugin
     this.fetchConfig = fetchConfig;

     // The application's configuration, including the
     // route definitions that we've declared in router-config.js
     this.appRouterConfig = appRouterConfig;
   };

   activate() {

       this.appRouterConfig.configure();
       this.fetchConfig.configure();

   };
}
