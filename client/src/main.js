//import 'bootstrap';
import config from './config/auth';

// Shim for IE8 and earlier
if (!Date.now) {
  Date.now = function() { return new Date().getTime(); }
}

export function configure(aurelia) {
  aurelia.use
    .standardConfiguration();
  //aurelia.use.developmentLogging();

  //Uncomment the line below to enable animation.
  aurelia.use.plugin('aurelia-animator-css');
  //if the css animator is enabled, add swap-order="after" to all router-view elements

  //Anyone wanting to use HTMLImports to load views, will need to install the following plugin.
  //aurelia.use.plugin('aurelia-html-import-template-loader')
  aurelia.use.plugin('aurelia-dialog');
  aurelia.use.plugin('aurelia-validation');

  aurelia.use.plugin('aurelia-auth', (baseConfig) => {
    baseConfig.configure(config);
  });

  aurelia.start().then(() => aurelia.setRoot());
}
