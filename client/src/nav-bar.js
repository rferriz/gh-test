import {bindable} from 'aurelia-framework';
import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';
import {BindingEngine} from 'aurelia-framework';
import {ClientSocket} from 'client-socket';

@inject (AuthService, BindingEngine, ClientSocket)

export class NavBar {
    _isAuthenticated = false;
    displayName = "";
    subscription = {};
    @bindable router = null;

    constructor(auth, bindingEngine, clientSocket) {
        var instance = this;
        this.auth = auth;
        this.bindingEngine = bindingEngine;
        this.clientSocket = clientSocket;
        this._isAuthenticated = this.auth.isAuthenticated();

        // Try to authenticate on authentication failed
        this.clientSocket.getSocket().on('not-authenticated', function() {
            console.log('Not authenticated event received');
            if (localStorage.getItem('aurelia_id_token') !== null) {
                instance.clientSocket.authenticate(localStorage.getItem('aurelia_id_token'));
            }
        });

        this.subscription = this.bindingEngine.propertyObserver(this, 'isAuthenticated')
            .subscribe((newValue, oldValue) => {
                if (this.isAuthenticated) {
                    this.clientSocket.authenticate(localStorage.getItem('aurelia_id_token'));
                }
            });
    }

    get isAuthenticated() {
        return this.auth.isAuthenticated();
    }

    deactivate() {
        this.subscription.dispose();
    }
}