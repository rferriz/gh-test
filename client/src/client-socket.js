import {inject} from 'aurelia-framework';

import io from 'socket.io-client';
import config from './config/websocket';

@inject(io, config)

export class ClientSocket {
    socket = {};
    user = {};

    constructor(io, webSocketConfig) {
        var client = this;
        this.socket = io.connect(webSocketConfig.url);
        this.socket.on('authenticated', function (payload) { client.handleOnAuthenticated(payload); });
        this.socket.on('me', function(payload) { client.handleOnMe(payload) });
    }

    handleOnAuthenticated(payload) {
        if (!payload.hasOwnProperty('user_id') || payload.user_id == null) {
            console.warn('Confusing authentication message received ... ');
        }

        if (this.socket)
            this.socket.emit('me');
    }

    handleOnMe(payload) {
        if (typeof payload !== 'undefined' && payload !== null) {
            this.user = payload;
        }
    }

    getSocket() {
        return this.socket;
    }

    getUserId() {
        if (this.user.hasOwnProperty('id'))
            return this.user.id;
        return null;
    }

    authenticate(token) {
        console.log('Send authentication request');
        this.socket.emit('authenticate', {idToken: token});
    }

    getCart(userId) {
        var message = {
            user_id: userId
        };

        this.socket.emit('get-cart', message);
    }

    addItem(shopListing, item) {
        var message = {
            id: shopListing,
            description: item.description
        };

        this.socket.emit('add-item', message);
    }

    updateItem(item) {
        var message = {
            id: item.id,
            description: item.description
        };

        this.socket.emit('update-item', message);
    }

    removeItem(item) {
        var message = {
            id: item.id
        };

        this.socket.emit('delete-item', message);
    }
}