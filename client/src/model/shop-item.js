export class ShopItem {
    constructor(uuid, description, created) {
        this.id = uuid;
        this.description = description;
        if (!created)
            created = Date.now();
        this.created = created;
    }

    setUuid(uuid) {
        this.id = uuid;
    }
}