import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';

@inject(AuthService)
export class Logout {

    constructor(authService) {
        this.authService = authService;
    };

    activate() {
        this.authService.logout('#/welcome')
            .then((response) => {
                localStorage.remove('access_token');
                localStorage.remove('aurelia_id_token');
                console.log("Logged Out");
            })
            .catch(error => {
                console.log("Error logging out")
            });
    };
}