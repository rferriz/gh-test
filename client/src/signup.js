import {inject} from 'aurelia-framework';
import {AuthService} from 'aurelia-auth';
import {ValidationControllerFactory, ValidationController, ValidationRules} from 'aurelia-validation';
import {BootstrapFormRenderer} from './bootstrap-form-renderer';


@inject(AuthService, ValidationControllerFactory)
export class Signup {
    controller = null;

    heading = 'Sign Up';

    // These view models will be given values from the signup form user input
    email = '';
    name = '';
    password = '';

    // Any signup errors will be reported by giving this view model a value in the catch block within the signup method
    signupError = '';

    constructor(auth,controllerFactory) {
        this.auth = auth;
        this.controller = controllerFactory.createForCurrentScope();
        this.controller.addRenderer(new BootstrapFormRenderer());
    };

    signup() {
        this.controller.validate()
            .then(result => {
                if (result.valid) {
                    return this.auth.signup(this.name, this.email, this.password)
                        .then((response) => {
                            localStorage.setItem('access_token', response.access_token);
                            console.log("Signed Up!");
                        })
                        .catch(error => {
                            this.signupError = error.statusText;
                        });
                }
            });
    };
}

ValidationRules
    .ensure(a => a.email).required().email()
    .ensure(a => a.name).minLength(4).required()
    .ensure(a => a.password).minLength(6).required()
    .on(Signup);