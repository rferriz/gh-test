import {DialogController} from 'aurelia-dialog';
import {inject} from 'aurelia-framework';

@inject(DialogController)

export class Dialog {
    constructor(controller) {
        this.controller = controller;
        this.answer = null;

        controller.settings.lock = false;
        controller.settings.centerHorizontally = true;
    }

    activate(question) {
        this.question = question;
    }
}
